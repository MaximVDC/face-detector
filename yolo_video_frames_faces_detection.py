import numpy as np
import time
import cv2
import os
import glob
import sys
from pathlib import Path
import csv

# Change the following variables to match custom project.

# INPUT:

# path to folder containing video files
INPUT_FOLDER='../Desktop/Videos2611'

# video file extension
VIDEO_EXTENSION=".avi"

# FRAME EXTRACTION:

# path to folder that will contain the extracted video frames
OUTPUT_PATH_FRAMES = '../Desktop/output_frames_face_temp'

# OUTPUT:

# Boolean value for deciding whether or not images with bounding boxes are desired.
BOUNDING_BOXES_ON_FRAMES = False
# If true, path to folder containing images with bounding boxes
DETECTION_OUTPUT_FOLDER='../Desktop/output_frames_face_temp/bb'

# Name and destination of the desired csv file
CSV_DESTINATION = "../Desktop/output_frames_face_temp/csv"
CSV_NAME="detection_results.csv"

# YOLO DETECTION parameters
LABELS_FILE='face_detector/faces_obj.names'
CONFIG_FILE='face_detector/yolov4_faces.cfg'
WEIGHTS_FILE='face_detector/yolov4_faces_best.weights'
CONFIDENCE_THRESHOLD=0.3

#####################################################################################################################

def extractVideoFrames(image_path, output_path):
    # Create a VideoCapture object and read from input file
    prefix = Path(image_path).stem
    video = cv2.VideoCapture(image_path)

    # Check if video opened successfully
    if not video.isOpened():
        print("Error opening video file")
        sys.exit

    # initialise counter for the frame names
    i = 0
    frame_no = 0
    print('frame extraction process started.')
    while True:
        # Read a new frame
        ok, frame = video.read()

        if not ok:
           break

        frame_name = prefix + '_frame' + str(frame_no)

        cv2.imwrite(output_path + frame_name + '.png', frame)

        frame_no += 1
    # Closes all the frames
    cv2.destroyAllWindows()
    print('video frame extraction finished')
    # When everything done,
    # release the video capture object
    video.release()


def detect_faces(writer):

    images_path = glob.glob(r"" + OUTPUT_PATH_FRAMES +"/*.png")

    LABELS = open(LABELS_FILE).read().strip().split("\n")

    np.random.seed(4)
    COLORS = np.random.randint(0, 255, size=(len(LABELS), 3), dtype="uint8")


    net = cv2.dnn.readNetFromDarknet(CONFIG_FILE, WEIGHTS_FILE)

    for img_path in images_path:
        image_name = os.path.basename(img_path)
        image = cv2.imread(img_path)
        (H, W) = image.shape[:2]

        # determine only the *output* layer names that we need from YOLO
        ln = net.getLayerNames()
        ln = [ln[i - 1] for i in net.getUnconnectedOutLayers()]

        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        net.setInput(blob)
        start = time.time()
        layerOutputs = net.forward(ln)
        end = time.time()

        print("[INFO] YOLO took {:.6f} seconds".format(end - start))

        # initialize our lists of detected bounding boxes, confidences, and
        # class IDs, respectively
        boxes = []
        confidences = []
        classIDs = []

        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:
                # extract the class ID and confidence (i.e., probability) of
                # the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > CONFIDENCE_THRESHOLD:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # update our list of bounding box coordinates, confidences,
                    # and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        # apply non-maxima suppression to suppress weak, overlapping bounding
        # boxes
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE_THRESHOLD,
            CONFIDENCE_THRESHOLD)

        # ensure at least one detection exists
        if len(idxs) > 0:
            # loop over the indexes we are keeping
            for i in idxs.flatten():
                # extract the bounding box coordinates
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])

                # add to csv
                writer.writerow([image_name.split('_frame')[0] + VIDEO_EXTENSION, image_name, str(x), str(y), str(w), str(h), confidences[i]])

                if BOUNDING_BOXES_ON_FRAMES:
                    color = [int(c) for c in COLORS[classIDs[i]]]
                    cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
                    text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
                    cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, color, 2)
                    cv2.putText(image, 'Number of faces: ' + str(len(idxs)), (30,25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        if BOUNDING_BOXES_ON_FRAMES:
            # show the output image
            if not os.path.exists(DETECTION_OUTPUT_FOLDER):
                os.makedirs(DETECTION_OUTPUT_FOLDER)
            cv2.imwrite(DETECTION_OUTPUT_FOLDER + os.path.basename(img_path).split('.')[0] + "_bb.png", image)



if not OUTPUT_PATH_FRAMES[len(OUTPUT_PATH_FRAMES)-1] == '/':
    path = OUTPUT_PATH_FRAMES
    OUTPUT_PATH_FRAMES = path + "/"

if (not DETECTION_OUTPUT_FOLDER[len(DETECTION_OUTPUT_FOLDER)-1] == '/') and BOUNDING_BOXES_ON_FRAMES:
    path = DETECTION_OUTPUT_FOLDER
    DETECTION_OUTPUT_FOLDER = path + "/"

if not CSV_DESTINATION[len(CSV_DESTINATION)-1] == '/':
    path = CSV_DESTINATION
    CSV_DESTINATION = path + "/"

for file in os.listdir(INPUT_FOLDER):

    if file.endswith(VIDEO_EXTENSION):

        if not os.path.exists(OUTPUT_PATH_FRAMES):
            os.makedirs(OUTPUT_PATH_FRAMES)
        path = os.path.join(INPUT_FOLDER, file)
        extractVideoFrames(path,OUTPUT_PATH_FRAMES)

if os.path.exists(OUTPUT_PATH_FRAMES):
    if not os.path.exists(CSV_DESTINATION):
        os.makedirs(CSV_DESTINATION)
    with open(CSV_DESTINATION + CSV_NAME, 'w', newline='') as f:
        writer = csv.writer(f)
        header = ['video', 'frame', 'bb_top_left_x', 'bb_top_left_y', 'bb_width', 'bb_height', 'conf']
        writer.writerow(header)
        detect_faces(writer)
        print("Detection process finished")
